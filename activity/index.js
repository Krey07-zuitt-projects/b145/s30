// console.log("Hello World!");


// 4

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json))


// 5

let result = fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {
	let titles = json.map(result => (result.title))
	console.log(titles)
});

// 6

fetch('http://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => {
	let completed = json.map(x => {
		let completed = {
			completed: x.completed,
			title: x.title
		} 
		return completed;
	})
	console.log(completed)
});

// 7

fetch('http://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'New Task to improve programming skills',
		body: 'Study Node.js',
		userId: 7
	})

})
.then((response) => response.json());
.then((json) => console.log(json));

// 8

fetch('http://jsonplaceholder.typicode.com/todos', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Update todo list',
		description: 'Update to do list',
		status: 'still in progress',
		dateCompleted: 'Not completed yet',
		body: 'This needs to be updated',
		userId: 7
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 10

fetch('http://jsonplaceholder.typicode.com/todos/7', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'New Task Update',
		completed: "completed",
		statusChanged: "01/26/2022"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 12

fetch('https://jsonplaceholder.typicode.com/posts/7', {
	method: 'DELETE'
});